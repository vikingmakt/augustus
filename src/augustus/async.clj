(ns augustus.async
  (:require [augustus.future :as future]
            [augustus.pools :as pools])
  (:import [java.util.concurrent CancellationException ExecutorService])
  (:gen-class))

(defn execute*
  ([f]
   (execute* pools/main-pool f))
  ([p f]
   (.execute ^ExecutorService p f)))

(defmacro execute [& body]
  `(execute* (fn [] (list ~@body))))

(defn add-callback [f & args]
  (execute (apply f args)))

(defn submit*
  ([f]
   (submit* pools/main-pool f))
  ([p f]
   (let [fut (future/new-future)]
     (execute* p #(try
                    (future/set-result fut (f))
                    (catch Throwable e
                      (future/set-exception fut e))))
     fut)))

(defmacro submit [& body]
  `(submit* (fn [] (last (list ~@body)))))

(defn call [f & args]
  (submit (apply f args)))

(defmacro custom-execute [p & body]
  `(execute* ~p (fn [] (list ~@body))))

(defn custom-add-callback [p f & args]
  (custom-execute p (apply f args)))

(defmacro custom-submit [p & body]
  `(submit* ~p (fn [] (last (list ~@body)))))

(defn custom-call [p f & args]
  (custom-submit p (apply f args)))
