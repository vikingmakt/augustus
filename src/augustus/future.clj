(ns augustus.future
  (:require [augustus.pools :as pools])
  (:import augustus.future.exceptions.AlreadyCompleted
           [clojure.lang IBlockingDeref IDeref IFn IPending]
           [java.util.concurrent CancellationException CountDownLatch Future ThreadPoolExecutor TimeoutException TimeUnit])
  (:gen-class))

(defprotocol IFuture
  (addCallback [this pool callback])
  (setException [this exception])
  (setResult [this result]))

(defn add-future-callback
  ([fut callback]
   (add-future-callback fut pools/main-pool callback))
  ([^augustus.future.IFuture fut pool callback]
   (.addCallback fut pool callback)))

(defn cancel [^Future fut]
  (.cancel fut false))

(defn set-exception [^augustus.future.IFuture fut ex]
  (.setException fut ex))

(defn set-result [^augustus.future.IFuture fut result]
  (.setResult fut result))

(defn- call-handler [callbacks]
  (when (seq callbacks)
    ((first callbacks))
    (recur (rest callbacks))))

(defn new-future
  ([]
   (let [d (new CountDownLatch 1)
         ex (atom nil)
         v (atom nil)
         cb (atom [])]
     (reify
       IDeref
       (deref [this]
         (.get this))
       IBlockingDeref
       (deref [this timeout-ms timeout-val]
         (try
           (.get this timeout-ms TimeUnit/MILLISECONDS)
           (catch TimeoutException e
             timeout-val)))
       IPending
       (isRealized [this]
         (.isDone this))
       Future
       (get [_]
         (.await d)
         (if (deref ex)
           (throw (deref ex))
           (deref v)))
       (get [this timeout unit]
         (if (.await d timeout unit)
           (if (deref ex)
             (throw (deref ex))
             (deref v))
           (throw (new TimeoutException))))
       (isCancelled [_]
         (instance? CancellationException (deref ex)))
       (isDone [_]
         (zero? (.getCount d)))
       (cancel [this _]
         (if (realized? this)
           false
           (do
             (.setException this (new CancellationException))
             true)))
       IFn
       (invoke [this v]
         (if (instance? Throwable v)
           (.setException this v)
           (.setResult this v)))
       IFuture
       (addCallback [this pool callback]
         (if (realized? this)
           (.execute ^ThreadPoolExecutor pool (fn [] (callback this)))
           (swap! cb conj #(.execute ^ThreadPoolExecutor pool (fn [] (callback this)))))
         true)
       (setException [this exception]
         (if (realized? this)
           (throw (new AlreadyCompleted)))
         (reset! ex exception)
         (.countDown d)
         (call-handler (deref cb))
         this)
       (setResult [this result]
         (if (realized? this)
           (throw (new AlreadyCompleted)))
         (reset! v result)
         (.countDown d)
         (call-handler (deref cb))
         this))))
  ([callback]
   (doto (new-future)
     (add-future-callback callback))))
