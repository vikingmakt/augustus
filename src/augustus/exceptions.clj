(ns augustus.exceptions
  (:gen-class
   :name augustus.future.exceptions.AlreadyCompleted
   :extends Exception))
