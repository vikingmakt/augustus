(ns augustus.pools
  (:import [java.util.concurrent Executors ExecutorService ThreadFactory ScheduledExecutorService])
  (:gen-class))

(defn- set-thread-name [^Thread t c]
  (.setName t (str "augustus-thread-" (swap! c inc))))

(defn- factory []
  (let [c (atom 0)]
    (proxy [ThreadFactory] []
      (newThread [^Runnable r]
        (doto (new Thread r)
          (set-thread-name c))))))

(defonce ^ExecutorService main-pool (Executors/newCachedThreadPool (factory)))

(defonce ^ScheduledExecutorService sched-pool (Executors/newSingleThreadScheduledExecutor (factory)))

(defn shutdown []
  (.shutdown main-pool)
  (.shutdown sched-pool))
