(ns augustus.timed
  (:require [augustus.async :as async]
            [augustus.future :as future]
            [augustus.pools :as pools])
  (:import java.lang.Runnable
           [java.util.concurrent ScheduledExecutorService TimeUnit])
  (:gen-class))

(def default-unit TimeUnit/MILLISECONDS)

(defn schedule*
  ([t f]
   (schedule* pools/sched-pool t f))
  ([^ScheduledExecutorService p ^Long t ^Runnable f]
   (.schedule p f t ^TimeUnit default-unit)))

(defmacro execute-after [t & body]
  `(schedule* ~t (fn [] (async/execute* (list ~@body)))))

(defn add-timeout [t f & args]
  (execute-after t (apply f args)))

(defn submit-after*
  ([t f]
   (submit-after* pools/sched-pool t f))
  ([p t f]
   (let [fut (future/new-future)]
     (schedule* p t (fn []
                      (async/execute
                       (try
                         (future/set-result fut (f))
                         (catch Exception e
                           (future/set-exception fut e))))))
     fut)))

(defmacro submit-after [t & body]
  `(submit-after* ~t (fn [] (last (list ~@body)))))

(defn call-after [t f & args]
  (submit-after t (apply f args)))

(defn add-delay-coroutine* [^Long t ^Runnable callback]
  (.scheduleWithFixedDelay ^ScheduledExecutorService pools/sched-pool callback t t ^TimeUnit default-unit))

(defn add-rate-coroutine* [^Long t ^Runnable callback]
  (.scheduleAtFixedRate ^ScheduledExecutorService pools/sched-pool callback t t ^TimeUnit default-unit))

(defmacro add-delay-coroutine [t & body]
  `(add-delay-coroutine* ~t (fn [] (list ~@body))))

(defmacro add-rate-coroutine [t & body]
  `(add-rate-coroutine* ~t (fn [] (list ~@body))))

(defmacro custom-execute-after [p t & body]
  `(schedule* ~p ~t (fn [] (async/execute* (list ~@body)))))

(defn custom-add-timeout [p t f & args]
  (custom-execute-after p t (apply f args)))

(defmacro custom-submit-after [p t & body]
  `(submit-after* ~p ~t (fn [] (last (list ~@body)))))

(defn custom-call-after [p t f & args]
  (custom-submit-after p t (apply f args)))

(defn custom-add-delay-coroutine* [p ^Long t ^Runnable callback]
  (.scheduleWithFixedDelay ^ScheduledExecutorService p callback t t ^TimeUnit default-unit))

(defn custom-add-rate-coroutine* [p ^Long t ^Runnable callback]
  (.scheduleAtFixedRate ^ScheduledExecutorService p callback t t ^TimeUnit default-unit))
