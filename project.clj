(defproject br.com.vikingmakt/augustus "0.1.12"
  :description "Thread pool functions"
  :url "https://gitlab.com/vikingmakt/augustus"
  :license {:name "BSD-2-Clause"}
  :aot [augustus.exceptions]
  :dependencies [[org.clojure/clojure "1.8.0"]])
