# AUGUSTUS

Thread pool functions

---

To enqueue a function:

```clojure
(ns example
  (:require [augustus.async :as async]))

(async/add-callback println "augustus")

"augustus"
```

To enqueue a function using a custom pool:

```clojure
(ns example
  (:require [augustus.async :as async]))

(async/custom-add-callback my-thread-pool println "augustus")

"augustus"
```

---

To call a function after a time:

```clojure
(ns example
  (:require [augustus.timed :as timed]))

(timed/add-timeout 5000 println "timed print")

"timed print"
```

---

Check the [API](./man/api.md) for more options
