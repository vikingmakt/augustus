# AUGUSTUS

Thread pool functions

## ASYNC

### EXECUTE

Enqueue a piece of code to main thread pool:

```clojure
>>> (require '[augustus.async :as async])

>>> (async/execute
      (println (+ 5 12)))

17
```

### ADD CALLBACK

Enqueue a function with args to main thread pool:

```clojure
>>> (require '[augustus.async :as async])

>>> (async/add-callback println "augustus")

"augustus"
```

### SUBMIT

Same as **execute** but returns a future that will be completed with the last form of the code:

```clojure
>>> (require '[augustus.async :as async])

>>> (def r (async/submit
      (range 0 10)))

>>> @r
    (0 1 2 3 4 5 6 7 8 9 10)
```

### CALL

Same as **add-callback** but returns a future:

```clojure
>>> (require '[augustus.async :as async])

>>> (def r (async/call * 10 50))

>>> @r
    500
```
